import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-atmospheric',
  templateUrl: './atmospheric.component.html',
  styleUrls: ['./atmospheric.component.scss']
})
export class AtmosphericComponent implements OnInit {
  @Input() item: any;

  constructor() {}

  ngOnInit() {}
}
