import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { LoaderComponent } from './loader/loader.component';
import { AtmosphericComponent } from './atmospheric/atmospheric.component';
import { DialogComponent } from './dialog/dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [FlexLayoutModule, MaterialModule, CommonModule, FormsModule, ReactiveFormsModule],
  declarations: [LoaderComponent, AtmosphericComponent, DialogComponent],
  entryComponents: [DialogComponent],
  exports: [LoaderComponent, AtmosphericComponent, FormsModule, ReactiveFormsModule]
})
export class SharedModule {}
