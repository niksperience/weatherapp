import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { RestService } from '../services/rest.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  value = '';

  constructor(public dialogRef: MatDialogRef<DialogComponent>) {
    // tslint:disable-next-line: deprecation
    dialogRef.beforeClose().subscribe(() => dialogRef.close(this.value));
  }

  ngOnInit() {}

  onSaveClick(): void {
    this.dialogRef.close();
  }
}
