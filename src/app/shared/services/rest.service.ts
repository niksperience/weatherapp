import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { environment } from '@env/environment';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';

@Injectable({
  providedIn: 'root'
})
export class RestService {
  apikey: string;
  dialogRef: MatDialogRef<DialogComponent>;

  // https://openweathermap.org/current

  constructor(private http: HttpClient, public dialog: MatDialog) {
    if (localStorage.getItem('apikey')) {
      this.apikey = localStorage.getItem('apikey');
    } else {
      this.openDialog();
    }
  }

  getCityWeather(city: string) {
    if (this.apikey) {
      return this.http.get(environment.serverUrl + '/data/2.5/weather?q=' + city + '&APPID=' + this.apikey, {
        responseType: 'json'
      });
    } else {
      this.openDialog();
      return false;
    }
  }

  openDialog() {
    this.dialogRef = this.dialog.open(DialogComponent);
    this.dialogRef.afterClosed().subscribe(key => {
      this.apikey = key;
      this.saveApikey(key);
    });
  }

  saveApikey(key: string) {
    localStorage.setItem('apikey', key);
  }
}
