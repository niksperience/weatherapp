import { Injectable } from '@angular/core';
import { City } from '../models/models';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {
  favorites: string[] = [];
  deleteFavotites = false;
  checked = false;

  constructor() {
    if (localStorage.getItem('favorites')) {
      this.favorites = JSON.parse(localStorage.getItem('favorites'));
    } else {
      this.favorites = ['Dubrovnik', 'Osijek', 'Zagreb', 'Las Vegas'];
    }
  }

  saveFavorites(newFavorites: City[]) {
    this.favorites = [];
    for (const city of newFavorites) {
      this.favorites.push(city.name);
    }
    localStorage.setItem('favorites', JSON.stringify(this.favorites));
  }

  getFavorites() {
    const favoritesObj: City[] = [];
    for (const city of this.favorites) {
      favoritesObj.push({ name: city, checked: false });
    }
    return favoritesObj;
  }
}
