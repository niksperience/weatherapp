import { Component, OnInit } from '@angular/core';
import { FavoritesService } from '@app/shared/services/favorites.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestService } from '@app/shared/services/rest.service';
import { City } from '@app/shared/models/models';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // loader to show while we fetch data
  isLoading = false;
  // used in defining form settings and validation
  citySearchFormGroup: FormGroup;
  cityformincomplete = false;
  // where we will save data from api
  cityWeather: any = [];
  // favorites citys
  favoriteCitys: City[] = [];

  // flag
  deleteFavorites = false;

  searchValue = '';
  // help flag if search return error
  cityNotFound = false;

  // compast directions translate from degrees
  compassSector = [
    'NORTH',
    'NORTH - NORTH EAST',
    'NORTH EAST',
    'EAST - NORTH EAST',
    'EAST',
    'EAST SOUTH EAST',
    'SOUTH EAST ',
    'SOUTH',
    'SOUTH - SOUTH WEST',
    'SOUTH WEST',
    'WEST - SOUTH WEST',
    'WEST',
    'WEST - NORTH WEST',
    'NORTH WEST',
    'NORTH - NORTH WEST',
    'NORTH'
  ];

  // href to img of wather states https://openweathermap.org/weather-conditions
  imageLink = 'http://openweathermap.org/img/wn/';

  constructor(
    private _formBuilder: FormBuilder,
    private apiService: RestService,
    private favoritesService: FavoritesService
  ) {
    // fetching favorite citys
    this.favoriteCitys = favoritesService.getFavorites();
  }

  ngOnInit() {
    this.isLoading = true;
    this.citySearchFormGroup = this._formBuilder.group({
      citySearchFormControl: ['', Validators.required]
    });
  }

  searchCity() {
    // check if form is valid
    if (this.citySearchFormGroup.valid) {
      this.isLoading = true;
      this.getCityWeather(this.citySearchFormGroup.controls.citySearchFormControl.value);
    } else {
      this.cityformincomplete = true;
    }
  }

  getCityWeather(cityName: string) {
    // setting flags
    this.cityNotFound = false;
    this.isLoading = true;

    // api call
    const call = this.apiService.getCityWeather(cityName);
    if (call) {
      call.subscribe({
        next: (data: any) => {
          this.cityWeather = data;
          this.isLoading = false;
        },
        error: (data: any) => {
          this.isLoading = false;
          if (data.status === 404) {
            this.cityNotFound = true;
            setTimeout(() => (this.cityNotFound = false), 4000);
          } else if (data.status === 401) {
            this.apiService.openDialog();
          }
        },
        complete: () => {}
      });
    }
  }

  saveFavorites(newFavorites: City[]) {
    this.favoritesService.saveFavorites(newFavorites);
    this.favoriteCitys = newFavorites;
  }

  deleteSelectedFavorites(cityToRemove?: string) {
    const newFavorites: City[] = [];
    for (const city of this.favoriteCitys) {
      // checking if city was marked for delete or star was removed
      if (!city.checked && !(city.name === cityToRemove)) {
        newFavorites.push(city);
      }
    }
    this.saveFavorites(newFavorites);
  }

  setFavorite(city: string) {
    // creating object
    const newCity: City = {
      name: city
    };
    this.favoriteCitys.push(newCity);
    this.saveFavorites(this.favoriteCitys);
  }

  isFavorite(citynName: string) {
    for (const city of this.favoriteCitys) {
      if (city.name === citynName) {
        return true;
      }
    }
    return false;
  }

  kelvinToFahrenheit(temp: number) {
    /*	℉=((K-273.15)*1.8)+32 */
    return Math.round(((temp - 273.15) * 1.8 + 32) * 100) / 100;
  }

  getWindDirection(deg: number) {
    /* 
    16 directions in compass stored in this.compassSector by order from 0 to 360;
    360°/16 = 22,5 -> teremine how much degrees is one direction;
    toFixed is used to remove decimals;
    -1 because its array;
    */

    // problem if we get -1
    // proxy is used to manupilate data
    // we will check of if it is negative add lenght of array

    const proxy = new Proxy(this.compassSector, {
      get(target: [], prop: any) {
        // checkig if ge got number
        if (!isNaN(prop)) {
          prop = parseInt(prop, 10);
          // if negative add lenght
          if (prop < 0) {
            prop += target.length;
          }
        }
        return target[prop];
      }
    });

    return proxy[Number((deg / 22.5).toFixed(0)) - 1];
  }
}
